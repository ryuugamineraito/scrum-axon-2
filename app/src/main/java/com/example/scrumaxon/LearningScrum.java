package com.example.scrumaxon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scrumaxon.Class.MyListAdapter;

public class LearningScrum extends AppCompatActivity {


    ListView simpleList;
    String countryList[] = {"SCRUM PILLARS", "SCRUM VALUES", "SCRUM ROLES", "SCRUM EVENTS", "SCRUM ARTIFACTS"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning_scrum);
        simpleList = (ListView)findViewById(R.id.list);
        MyListAdapter adapter=new MyListAdapter(this, countryList);
        simpleList.setAdapter(adapter);


        ImageView img = (ImageView) findViewById(R.id.back_row);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });


//        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                // Get the selected item text from ListView
//
//            }
//
//        });
    }
}
