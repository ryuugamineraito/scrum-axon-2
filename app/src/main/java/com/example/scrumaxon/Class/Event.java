package com.example.scrumaxon.Class;

public class Event {
    private String Title;
    private String Description;

    public Event() {
    }

    public Event(String title, String des) {
        this.Title = title;
        this.Description = des;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String des) {
        this.Description = des;
    }

}