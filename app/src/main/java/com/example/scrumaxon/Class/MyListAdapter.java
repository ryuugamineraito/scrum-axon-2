package com.example.scrumaxon.Class;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.example.scrumaxon.DescriptionPage;
import com.example.scrumaxon.DetailPage;
import com.example.scrumaxon.R;

public class MyListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] title;

    public MyListAdapter(Activity context, String[] title) {
        super(context, R.layout.layout, title);

        this.context=context;
        this.title = title;

    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.layout, null,true);
        Button button = (Button) rowView.findViewById(R.id.textView);
        button.setText(title[position]);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailPage.class);
                String selectedItem = title[position];
                intent.putExtra("message", selectedItem);
                context.startActivity(intent);
            }
        });
        return rowView;
    };
}