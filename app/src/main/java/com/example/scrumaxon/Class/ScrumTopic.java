package com.example.scrumaxon.Class;

import java.util.List;

public class ScrumTopic {
    static  String title;
    List <ScrumDetail> detailList;

    public ScrumTopic(String title, List<ScrumDetail> detailList) {
        this.title = title;
        this.detailList = detailList;
    }

    public String getTitle() {
        return title;
    }

    public List<ScrumDetail> getDetailList() {
        return detailList;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDetailList(List<ScrumDetail> detailList) {
        this.detailList = detailList;
    }
}
