package com.example.scrumaxon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.scrumaxon.Class.ScrumDetail;

import java.util.List;

public class ScrumDetailAdapter extends RecyclerView.Adapter<ScrumDetailAdapter.myViewHolder> {
    private Context context;
    private List<ScrumDetail> scrumDetailList;

    public ScrumDetailAdapter(List<ScrumDetail> listscrum,Context context) {
        this.context = context;
        this.scrumDetailList = listscrum;
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.item_pillar, viewGroup, false);
        return new myViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final myViewHolder myViewHolder, final int i) {
        myViewHolder.bt.setText(scrumDetailList.get(i).getTitle());
        myViewHolder.bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String detail_srcum = scrumDetailList.get(i).getTitle();
                String descripton_Scrum = scrumDetailList.get(i).getDescription();
                Intent intent = new Intent(context, DescriptionPage.class);
                Bundle bundle = new Bundle();

                bundle.putString("EXTRA_DETAIL_DES", descripton_Scrum);
                bundle.putString("EXTRA_DETAIL_TITLE", detail_srcum);

                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return scrumDetailList.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView tv;
        Button bt;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            bt = (Button) itemView.findViewById(R.id.detail_title_button);
        }
    }
}