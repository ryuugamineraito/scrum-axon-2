package com.example.scrumaxon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DescriptionPage extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.descriptionpage);
        ImageView img = (ImageView) findViewById(R.id.back_row3);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        String detail_des = bundle.getString("EXTRA_DETAIL_DES");
        String detail_title = bundle.getString("EXTRA_DETAIL_TITLE");

        TextView title, detail;
        title = (TextView)findViewById(R.id.detail_title_name);
        detail=(TextView)findViewById(R.id.detail_title_textview);
        title.setText(detail_title);
        detail.setText(detail_des);

    }
}
