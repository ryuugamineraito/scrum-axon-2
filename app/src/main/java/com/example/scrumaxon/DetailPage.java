package com.example.scrumaxon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.scrumaxon.Class.ScrumDetail;
import com.example.scrumaxon.Class.ScrumTopic;

import java.util.ArrayList;
import java.util.List;

public class DetailPage extends AppCompatActivity {
    private TextView tv;
    public static final String MESSAGE ="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view_detail);
        Intent intent = new Intent();

        ImageView img = (ImageView) findViewById(R.id.back_row2);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        String title_detail= (String) getIntent().getExtras().getString("message");

        Toast.makeText(this.getApplicationContext(), title_detail, Toast.LENGTH_LONG).show();
        tv = (TextView)findViewById(R.id.detail_title);
        tv.setText(title_detail);

        List<ScrumDetail> scrumDetailList1 = new ArrayList<>();
        List<ScrumDetail> scrumDetailList2 = new ArrayList<>();
        List<ScrumDetail> scrumDetailList3 = new ArrayList<>();
        List<ScrumDetail> scrumDetailList4 = new ArrayList<>();
        List<ScrumDetail> scrumDetailList5 = new ArrayList<>();

        scrumDetailList1.add(new ScrumDetail("TRANSPARENCY", "Significant aspects of the process must be visible to those responsible for the outcome. Transparency requires those aspects be defined by a common standard so observers share a common understanding of what is being seen.\n" +
                "\n" +
                "For example:\n" +
                "\n" +
                "A common language referring to the process must be shared by all participants; and,\n" +
                "Those performing the work and those inspecting the resulting increment must share a common definition of \"Done\"."));
        scrumDetailList1.add(new ScrumDetail("INSPECTION", "Scrum users must frequently inspect Scrum artifacts and progress toward a Sprint Goal to detect undesirable variances. Their inspection should not be so frequent that inspection gets in the way of the work. Inspections are most beneficial when diligently performed by skilled inspectors at the point of work."));
        scrumDetailList1.add(new ScrumDetail("Adaptation", "If an inspector determines that one or more aspects of a process deviate outside acceptable limits, and that the resulting product will be unacceptable, the process or the material being processed must be adjusted. An adjustment must be made as soon as possible to minimize further deviation.\n" +
                "\n" +
                "Scrum prescribes four formal events for inspection and adaptation, as described in the Scrum Events section of this document:\n" +
                "\n" +
                "Sprint Planning\n" +
                "Daily Scrum\n" +
                "Sprint Review\n" +
                "Sprint Retrospective\n"));
        scrumDetailList2.add(new ScrumDetail("VALUES", "When the values of commitment, courage, focus, openness and respect are embodied and lived by the Scrum Team, the Scrum pillars of transparency, inspection, and adaptation come to life and build trust for everyone. The Scrum Team members learn and explore those values as they work with the Scrum events, roles and artifacts.\n" +
                "\n" +
                "Successful use of Scrum depends on people becoming more proficient in living these five values. People personally commit to achieving the goals of the Scrum Team. The Scrum Team members have courage to do the right thing and work on tough problems. Everyone focuses on the work of the Sprint and the goals of the Scrum Team. The Scrum Team and its stakeholders agree to be open about all the work and the challenges with performing the work. Scrum Team members respect each other to be capable, independent people."));
        scrumDetailList3.add(new ScrumDetail("PRODUCT OWNER","The Product Owner is responsible for maximizing the value of the product resulting from work of the Development Team. How this is done may vary widely across organizations, Scrum Teams, and individuals.\n" +
                "\n" +
                "The Product Owner is the sole person responsible for managing the Product Backlog. Product Backlog management includes:\n" +
                "\n" +
                "Clearly expressing Product Backlog items;\n" +
                "Ordering the items in the Product Backlog to best achieve goals and missions;\n" +
                "Optimizing the value of the work the Development Team performs;\n" +
                "Ensuring that the Product Backlog is visible, transparent, and clear to all, and shows what the Scrum Team will work on next; and,\n" +
                "Ensuring the Development Team understands items in the Product Backlog to the level needed.\n" +
                "The Product Owner may do the above work, or have the Development Team do it. However, the Product Owner remains accountable.\n" +
                "\n" +
                "The Product Owner is one person, not a committee. The Product Owner may represent the desires of a committee in the Product Backlog, but those wanting to change a Product Backlog item’s priority must address the Product Owner.\n" +
                "\n" +
                "For the Product Owner to succeed, the entire organization must respect his or her decisions. The Product Owner’s decisions are visible in the content and ordering of the Product Backlog. No one can force the Development Team to work from a different set of requirements."));
        scrumDetailList3.add(new ScrumDetail("THE DEVELOPMENT TEAM","The Development Team consists of professionals who do the work of delivering a potentially releasable Increment of \"Done\" product at the end of each Sprint. A \"Done\" increment is required at the Sprint Review. Only members of the Development Team create the Increment.\n" +
                "\n" +
                "Development Teams are structured and empowered by the organization to organize and manage their own work. The resulting synergy optimizes the Development Team’s overall efficiency and effectiveness.\n" +
                "\n" +
                "Development Teams have the following characteristics:\n" +
                "\n" +
                "They are self-organizing. No one (not even the Scrum Master) tells the Development Team how to turn Product Backlog into Increments of potentially releasable functionality;\n" +
                "Development Teams are cross-functional, with all the skills as a team necessary to create a product Increment;\n" +
                "Scrum recognizes no titles for Development Team members, regardless of the work being performed by the person;\n" +
                "Scrum recognizes no sub-teams in the Development Team, regardless of domains that need to be addressed like testing, architecture, operations, or business analysis; and,\n" +
                "Individual Development Team members may have specialized skills and areas of focus, but accountability belongs to the Development Team as a whole."));
        scrumDetailList3.add(new ScrumDetail("SCRUM MASTER","The Scrum Master is responsible for promoting and supporting Scrum as defined in the Scrum Guide. Scrum Masters do this by helping everyone understand Scrum theory, practices, rules, and values.\n" +
                "\n" +
                "The Scrum Master is a servant-leader for the Scrum Team. The Scrum Master helps those outside the Scrum Team understand which of their interactions with the Scrum Team are helpful and which aren’t. The Scrum Master helps everyone change these interactions to maximize the value created by the Scrum Team.\n" +
                "\n" +
                "Scrum Master Service to the Product Owner\n" +
                "The Scrum Master serves the Product Owner in several ways, including:\n" +
                "\n" +
                "Ensuring that goals, scope, and product domain are understood by everyone on the Scrum Team as well as possible;\n" +
                "Finding techniques for effective Product Backlog management;\n" +
                "Helping the Scrum Team understand the need for clear and concise Product Backlog items;\n" +
                "Understanding product planning in an empirical environment;\n" +
                "Ensuring the Product Owner knows how to arrange the Product Backlog to maximize value;\n" +
                "Understanding and practicing agility; and,\n" +
                "Facilitating Scrum events as requested or needed."));
        scrumDetailList4.add(new ScrumDetail("THE SPRINT","The heart of Scrum is a Sprint, a time-box of one month or less during which a \"Done\", useable, and potentially releasable product Increment is created. Sprints have consistent durations throughout a development effort. A new Sprint starts immediately after the conclusion of the previous Sprint.\n" +
                        "\n" +
                        "Sprints contain and consist of the Sprint Planning, Daily Scrums, the development work, the Sprint Review, and the Sprint Retrospective.\n" +
                        "\n" +
                        "During the Sprint:\n" +
                        "\n" +
                        "No changes are made that would endanger the Sprint Goal;\n" +
                        "Quality goals do not decrease; and,\n" +
                        "Scope may be clarified and re-negotiated between the Product Owner and Development Team as more is learned.\n" +
                        "Each Sprint may be considered a project with no more than a one-month horizon. Like projects, Sprints are used to accomplish something. Each Sprint has a goal of what is to be built, a design and flexible plan that will guide building it, the work, and the resultant product increment.\n" +
                        "\n" +
                        "Sprints are limited to one calendar month. When a Sprint’s horizon is too long the definition of what is being built may change, complexity may rise, and risk may increase. Sprints enable predictability by ensuring inspection and adaptation of progress toward a Sprint Goal at least every calendar month. Sprints also limit risk to one calendar month of cost.\n" +
                        "\n" +
                        "Cancelling a Sprint\n" +
                        "A Sprint can be cancelled before the Sprint time-box is over. Only the Product Owner has the authority to cancel the Sprint, although he or she may do so under influence from the stakeholders, the Development Team, or the Scrum Master.\n" +
                        "\n" +
                        "A Sprint would be cancelled if the Sprint Goal becomes obsolete. This might occur if the company changes direction or if market or technology conditions change. In general, a Sprint should be cancelled if it no longer makes sense given the circumstances. But, due to the short duration of Sprints, cancellation rarely makes sense.\n" +
                        "\n" +
                        "When a Sprint is cancelled, any completed and \"Done\" Product Backlog items are reviewed. If part of the work is potentially releasable, the Product Owner typically accepts it. All incomplete Product Backlog Items are re-estimated and put back on the Product Backlog. The work done on them depreciates quickly and must be frequently re-estimated.\n" +
                        "\n" +
                        "Sprint cancellations consume resources, since everyone regroups in another Sprint Planning to start another Sprint. Sprint cancellations are often traumatic to the Scrum Team, and are very uncommon."));
        scrumDetailList4.add(new ScrumDetail("SPRINT PLANNING","The work to be performed in the Sprint is planned at the Sprint Planning. This plan is created by the collaborative work of the entire Scrum Team.\n" +
                "\n" +
                "Sprint Planning is time-boxed to a maximum of eight hours for a one-month Sprint. For shorter Sprints, the event is usually shorter. The Scrum Master ensures that the event takes place and that attendants understand its purpose. The Scrum Master teaches the Scrum Team to keep it within the time-box.\n" +
                "\n" +
                "Sprint Planning answers the following:\n" +
                "\n" +
                "What can be delivered in the Increment resulting from the upcoming Sprint?\n" +
                "How will the work needed to deliver the Increment be achieved?"));
        scrumDetailList4.add(new ScrumDetail("DAILY SCRUM", "The Daily Scrum is a 15-minute time-boxed event for the Development Team. The Daily Scrum is held every day of the Sprint. At it, the Development Team plans work for the next 24 hours. This optimizes team collaboration and performance by inspecting the work since the last Daily Scrum and forecasting upcoming Sprint work. The Daily Scrum is held at the same time and place each day to reduce complexity.\n" +
                "\n" +
                "The Development Team uses the Daily Scrum to inspect progress toward the Sprint Goal and to inspect how progress is trending toward completing the work in the Sprint Backlog. The Daily Scrum optimizes the probability that the Development Team will meet the Sprint Goal. Every day, the Development Team should understand how it intends to work together as a self-organizing team to accomplish the Sprint Goal and create the anticipated Increment by the end of the Sprint.\n" +
                "\n" +
                "The structure of the meeting is set by the Development Team and can be conducted in different ways if it focuses on progress toward the Sprint Goal. Some Development Teams will use questions, some will be more discussion based. Here is an example of what might be used:\n" +
                "\n" +
                "What did I do yesterday that helped the Development Team meet the Sprint Goal?\n" +
                "What will I do today to help the Development Team meet the Sprint Goal?\n" +
                "Do I see any impediment that prevents me or the Development Team from meeting the Sprint Goal?\n" +
                "The Development Team or team members often meet immediately after the Daily Scrum for detailed discussions, or to adapt, or replan, the rest of the Sprint’s work.\n" +
                "\n" +
                "The Scrum Master ensures that the Development Team has the meeting, but the Development Team is responsible for conducting the Daily Scrum. The Scrum Master teaches the Development Team to keep the Daily Scrum within the 15-minute time-box.\n" +
                "\n" +
                "The Daily Scrum is an internal meeting for the Development Team. If others are present, the Scrum Master ensures that they do not disrupt the meeting.\n" +
                "\n" +
                "Daily Scrums improve communications, eliminate other meetings, identify impediments to development for removal, highlight and promote quick decision-making, and improve the Development Team’s level of knowledge. This is a key inspect and adapt meeting."));
        scrumDetailList4.add(new ScrumDetail("SPRINT REVIEW", "A Sprint Review is held at the end of the Sprint to inspect the Increment and adapt the Product Backlog if needed. During the Sprint Review, the Scrum Team and stakeholders collaborate about what was done in the Sprint. Based on that and any changes to the Product Backlog during the Sprint, attendees collaborate on the next things that could be done to optimize value. This is an informal meeting, not a status meeting, and the presentation of the Increment is intended to elicit feedback and foster collaboration.\n" +
                "\n" +
                "This is at most a four-hour meeting for one-month Sprints. For shorter Sprints, the event is usually shorter. The Scrum Master ensures that the event takes place and that attendees understand its purpose. The Scrum Master teaches everyone involved to keep it within the time-box.\n" +
                "\n" +
                "The Sprint Review includes the following elements:\n" +
                "\n" +
                "Attendees include the Scrum Team and key stakeholders invited by the Product Owner;\n" +
                "The Product Owner explains what Product Backlog items have been \"Done\" and what has not been \"Done\";\n" +
                "The Development Team discusses what went well during the Sprint, what problems it ran into, and how those problems were solved;\n" +
                "The Development Team demonstrates the work that it has \"Done\" and answers questions about the Increment;\n" +
                "The Product Owner discusses the Product Backlog as it stands. He or she projects likely target and delivery dates based on progress to date (if needed);\n" +
                "The entire group collaborates on what to do next, so that the Sprint Review provides valuable input to subsequent Sprint Planning;\n" +
                "Review of how the marketplace or potential use of the product might have changed what is the most valuable thing to do next; and,\n" +
                "Review of the timeline, budget, potential capabilities, and marketplace for the next anticipated releases of functionality or capability of the product.\n" +
                "The result of the Sprint Review is a revised Product Backlog that defines the probable Product Backlog items for the next Sprint. The Product Backlog may also be adjusted overall to meet new opportunities."));
        scrumDetailList4.add(new ScrumDetail("SPRINT RETROSPECTIVE","The Sprint Retrospective is an opportunity for the Scrum Team to inspect itself and create a plan for improvements to be enacted during the next Sprint.\n" +
                "\n" +
                "The Sprint Retrospective occurs after the Sprint Review and prior to the next Sprint Planning. This is at most a three-hour meeting for one-month Sprints. For shorter Sprints, the event is usually shorter. The Scrum Master ensures that the event takes place and that attendants understand its purpose.\n" +
                "\n" +
                "The Scrum Master ensures that the meeting is positive and productive. The Scrum Master teaches all to keep it within the time-box. The Scrum Master participates as a peer team member in the meeting from the accountability over the Scrum process.\n" +
                "\n" +
                "The purpose of the Sprint Retrospective is to:\n" +
                "\n" +
                "Inspect how the last Sprint went with regards to people, relationships, process, and tools;\n" +
                "Identify and order the major items that went well and potential improvements; and,\n" +
                "Create a plan for implementing improvements to the way the Scrum Team does its work.\n" +
                "The Scrum Master encourages the Scrum Team to improve, within the Scrum process framework, its development process and practices to make it more effective and enjoyable for the next Sprint. During each Sprint Retrospective, the Scrum Team plans ways to increase product quality by improving work processes or adapting the definition of \"Done\", if appropriate and not in conflict with product or organizational standards.\n" +
                "\n" +
                "By the end of the Sprint Retrospective, the Scrum Team should have identified improvements that it will implement in the next Sprint. Implementing these improvements in the next Sprint is the adaptation to the inspection of the Scrum Team itself. Although improvements may be implemented at any time, the Sprint Retrospective provides a formal opportunity to focus on inspection and adaptation."));
        scrumDetailList5.add(new ScrumDetail("PRODUCT BACKLOG", "The Product Backlog is an ordered list of everything that is known to be needed in the product. It is the single source of requirements for any changes to be made to the product. The Product Owner is responsible for the Product Backlog, including its content, availability, and ordering.\n" +
                "\n" +
                "A Product Backlog is never complete. The earliest development of it lays out the initially known and best-understood requirements. The Product Backlog evolves as the product and the environment in which it will be used evolves. The Product Backlog is dynamic; it constantly changes to identify what the product needs to be appropriate, competitive, and useful. If a product exists, its Product Backlog also exists.\n" +
                "\n" +
                "The Product Backlog lists all features, functions, requirements, enhancements, and fixes that constitute the changes to be made to the product in future releases. Product Backlog items have the attributes of a description, order, estimate, and value. Product Backlog items often include test descriptions that will prove its completeness when \"Done\".\n" +
                "\n" +
                "As a product is used and gains value, and the marketplace provides feedback, the Product Backlog becomes a larger and more exhaustive list. Requirements never stop changing, so a Product Backlog is a living artifact. Changes in business requirements, market conditions, or technology may cause changes in the Product Backlog.\n" +
                "\n" +
                "Multiple Scrum Teams often work together on the same product. One Product Backlog is used to describe the upcoming work on the product. A Product Backlog attribute that groups items may then be employed.\n" +
                "\n" +
                "Product Backlog refinement is the act of adding detail, estimates, and order to items in the Product Backlog. This is an ongoing process in which the Product Owner and the Development Team collaborate on the details of Product Backlog items. During Product Backlog refinement, items are reviewed and revised. The Scrum Team decides how and when refinement is done. Refinement usually consumes no more than 10% of the capacity of the Development Team. However, Product Backlog items can be updated at any time by the Product Owner or at the Product Owner’s discretion.\n" +
                "\n" +
                "Higher ordered Product Backlog items are usually clearer and more detailed than lower ordered ones. More precise estimates are made based on the greater clarity and increased detail; the lower the order, the less detail. Product Backlog items that will occupy the Development Team for the upcoming Sprint are refined so that any one item can reasonably be \"Done\" within the Sprint time-box. Product Backlog items that can be \"Done\" by the Development Team within one Sprint are deemed \"Ready\" for selection in a Sprint Planning. Product Backlog items usually acquire this degree of transparency through the above described refining activities.\n" +
                "\n" +
                "The Development Team is responsible for all estimates. The Product Owner may influence the Development Team by helping it understand and select trade-offs, but the people who will perform the work make the final estimate."));
        scrumDetailList5.add(new ScrumDetail("SPRINT BACKLOG","The Sprint Backlog is the set of Product Backlog items selected for the Sprint, plus a plan for delivering the product Increment and realizing the Sprint Goal. The Sprint Backlog is a forecast by the Development Team about what functionality will be in the next Increment and the work needed to deliver that functionality into a \"Done\" Increment.\n" +
                "\n" +
                "The Sprint Backlog makes visible all the work that the Development Team identifies as necessary to meet the Sprint Goal. To ensure continuous improvement, it includes at least one high priority process improvement identified in the previous Retrospective meeting.\n" +
                "\n" +
                "The Sprint Backlog is a plan with enough detail that changes in progress can be understood in the Daily Scrum. The Development Team modifies the Sprint Backlog throughout the Sprint, and the Sprint Backlog emerges during the Sprint. This emergence occurs as the Development Team works through the plan and learns more about the work needed to achieve the Sprint Goal.\n" +
                "\n" +
                "As new work is required, the Development Team adds it to the Sprint Backlog. As work is performed or completed, the estimated remaining work is updated. When elements of the plan are deemed unnecessary, they are removed. Only the Development Team can change its Sprint Backlog during a Sprint. The Sprint Backlog is a highly visible, real-time picture of the work that the Development Team plans to accomplish during the Sprint, and it belongs solely to the Development Team."));
        scrumDetailList5.add(new ScrumDetail("INCREMENT","The Increment is the sum of all the Product Backlog items completed during a Sprint and the value of the increments of all previous Sprints. At the end of a Sprint, the new Increment must be \"Done,\" which means it must be in useable condition and meet the Scrum Team’s definition of \"Done\". An increment is a body of inspectable, done work that supports empiricism at the end of the Sprint. The increment is a step toward a vision or goal. The increment must be in useable condition regardless of whether the Product Owner decides to release it."));

        List <ScrumDetail> endlistDetail = new ArrayList<>();
        switch (title_detail){
            case "SCRUM PILLARS":
                endlistDetail= scrumDetailList1;
                break;
            case "SCRUM VALUES":
                endlistDetail = scrumDetailList2;
                break;
            case "SCRUM ROLES":
                endlistDetail = scrumDetailList3;
                break;
            case "SCRUM EVENTS":
                endlistDetail = scrumDetailList4;
                break;
            case "SCRUM ARTIFACTS":
                endlistDetail = scrumDetailList5;
                break;
            default:
                break;
        }


        RecyclerView DetailRecyclerview = (RecyclerView)findViewById(R.id.detail_recyclerview);
        ScrumDetailAdapter scrumAdapter= new ScrumDetailAdapter(endlistDetail, this);
        LinearLayoutManager cLayout = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        DetailRecyclerview.setAdapter(scrumAdapter);
        DetailRecyclerview.setLayoutManager(cLayout);


    }
}
